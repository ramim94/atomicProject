<?php
use App\BITM\SEIP136876\BookTitle\BookTitle;
use App\utils\Utility;
include_once ("../../../vendor/autoload.php");

session_start();

$book= new BookTitle();

$book->prepare($_POST);
if($book->update()){
    $_SESSION['message']="<div class=\"alert alert-info\">
  <strong>Success!</strong> Data successfully updated
</div>";
    Utility::redirect("index.php");
}else{
    $_SESSION['message']="<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Update Unsuccessful
</div>";
    Utility::redirect("index.php");
}
