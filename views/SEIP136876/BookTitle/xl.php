<?php
include_once ("../../../vendor/autoload.php");
include_once ("../../../vendor/phpoffice/phpexcel/Classes/PHPExcel.php");

$objPHPExcel = new PHPExcel();

$book=new \App\BITM\SEIP136876\BookTitle\BookTitle();
$alldata=$book->index();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Mohaimanul Chowdhury")
    ->setLastModifiedBy("Mohaimanul Chowdhury")
    ->setTitle("Test Document")
    ->setSubject("Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");


// Add some data
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', 'SL')
    ->setCellValue('B1', 'ID')
    ->setCellValue('C1', 'Title');
$sl=0;
$counter=1;
foreach($alldata as $data):
    $sl++;
    $counter++;
// Miscellaneous glyphs, UTF-8
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$counter, $sl)
        ->setCellValue('B'.$counter, $data->id)
        ->setCellValue('C'.$counter, $data->title);

// Rename worksheet
    $objPHPExcel->getActiveSheet()->setTitle('Simple');

endforeach;
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="01simple.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

