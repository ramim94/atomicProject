<?php
use App\BITM\SEIP136876\BookTitle;
use App\utils\Utility;
include_once('../../../vendor/autoload.php');

session_start();

$book= new BookTitle\BookTitle();
$book->prepare($_POST);
if($book->store()){
    $_SESSION['message']="<div class=\"alert alert-success\">
  <strong>Success!</strong> Data successfully stored
</div>";
    Utility::redirect("index.php");
}
else{
    $_SESSION['message']="<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Failed to store data
</div>";
    Utility::redirect("index.php");
}
