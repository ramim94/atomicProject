<?php
use App\BITM\SEIP136876\BookTitle\BookTitle;

include_once('../../../vendor/autoload.php');

$book= new BookTitle();
$book->prepare($_GET);

$oneBook=$book->view();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Single Book View</h2>
    <ul class="list-group">
        <li class="list-group-item"> <strong>Book ID:</strong> <?php echo $oneBook["id"]?> </li>
        <li class="list-group-item"> <strong>Book Title:</strong> <?php echo $oneBook["title"]?> </li>
        <li class="list-group-item"> <strong>Will be Emailed to:</strong> <?php echo $oneBook["toemail"]?> </li>
    </ul>

    <a href="index.php" class="btn btn-info" role="button">All books </a>
</div>

</body>
</html>


