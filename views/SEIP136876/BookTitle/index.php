<?php
session_start();
use App\utils\Utility;
use App\utils\Message;
use App\BITM\SEIP136876\BookTitle\BookTitle;
include_once("../../../vendor/autoload.php");
$city= new BookTitle();
//$bookinfos=$allbook->index();

$availableTitle=$city->getAllTitle();

$comma_separated= '"'.implode('","',$availableTitle).'"';

if(array_key_exists('itemPerPage',$_SESSION)) {
    if (array_key_exists('itemPerPage', $_GET)) {
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
    }
}
else{
    $_SESSION['itemPerPage']=5;
}

//Utility::dd($_SESSION['itemPerPage']);

$itemPerPage=$_SESSION['itemPerPage'];
$totalItem=$city->count();



$totalPage=ceil($totalItem/$itemPerPage);
//Utility::dd($itemPerPage);
$pagination="";


if(array_key_exists('pageNumber',$_GET)){
    $pageNumber=$_GET['pageNumber'];
}else{
    $pageNumber=1;
}
for($i=1;$i<=$totalPage;$i++){
    $class=($pageNumber==$i)?"active":"";
    $pagination.="<li class='$class'><a href='index.php?pageNumber=$i'>$i</a></li>";
}

$pageStartFrom=$itemPerPage*($pageNumber-1);
if(strtoupper($_SERVER['REQUEST_METHOD'])=='GET'){
    $bookinfos=$city->paginator($pageStartFrom,$itemPerPage);

}if(strtoupper($_SERVER['REQUEST_METHOD'])=='POST'){
    $city->prepare($_POST);
    $bookinfos=$city->index();
}
if((strtoupper($_SERVER['REQUEST_METHOD']=='GET'))&& isset($_GET['search'])) {
     $city->prepare($_GET);
    $bookinfos = $city->index();
}


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>BookTitle List</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>All book list</h2>

    <a href="create.php" class="btn btn-info" role="button">Create New</a>
    <a href="trashed.php" class="btn btn-info" role="button">See Trash</a>
    <a href="../../../index.php" class="btn btn-primary" role="button">View Main Page</a>
    <br>
    <br>

    <form method="post" action="index.php">
        <input type="text" class="form-control" id="fTitle" name="filterTitle" placeholder="Filter by title">
        <p>OR,</p>
        <input type="text" class="form-control" id="fDesc" name="filterDesc" placeholder="Filter by description">
        <input type="submit" class="btn-default">
    </form>
    <br>
    <br>

    <form action="index.php" method="get">
    <label>Search: </label>
    <input type="text" name="search" value="">
    <button type="submit">Search</button>
    </form>

    <form role="form">
        <div class="form-group">
            <label for="sel1">Select how many items you want to show (select one):</label>
            <select class="form-control" id="sel1" name="itemPerPage">

                <option <?php if($itemPerPage==5) echo "selected" ?> >5</option>
                <option <?php if($itemPerPage==10) echo "selected" ?> >10</option>
                <option <?php if($itemPerPage==15) echo "selected" ?> >15</option>
                <option <?php if($itemPerPage==20) echo "selected" ?> >20</option>
                <option <?php if($itemPerPage==25) echo "selected" ?> >25</option>
            </select>
            <button type="submit">Go!</button>

        </div>
    </form>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Sl.</th>
            <th>Id</th>
            <th>Book Title</th>
            <th>Email To</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <div id="message">
        <?php $sl=1;
        echo Message::showMessage();
        foreach ($bookinfos as $book){ ?>
        </div>
        <tr>
            <td> <?php echo $sl+$pageStartFrom?></td>
            <td> <?php echo $book["id"]?> </td>
            <td><?php echo $book["title"]?> </td>
            <td><?php echo $book["toemail"]?> </td>
            <td><a href="view.php?id=<?php echo $book["id"]  ?>" class="btn btn-primary" role="button">View</a>
                <a href= <?php echo "edit.php?id=".$book["id"]?> role="button" class="btn btn-success">Edit</a>
                <a href="delete.php?id=<?php echo $book["id"]  ?>" role="button" class="btn btn-danger">Delete</a>
                <a href="trash.php?id=<?php echo $book["id"]  ?>" role="button" class="btn btn-warning">Trash</a>
                <a href="singleEmail.php?id=<?php echo $book["id"]  ?>&sl=<?php echo $sl+$pageStartFrom ?>" role="button" class="btn btn-primary">Email This</a>
            </td>
        </tr>
       <?php $sl++; } ?>
        </tbody>
    </table>
    <?php if(strtoupper($_SERVER['REQUEST_METHOD']=='GET')) { ?>
    <ul class="pagination">
        <li class=" <?php if($pageNumber==1) echo "hidden"?> "><a href="index.php?pageNumber=<?php echo $pageNumber-1?>">Prev</a></li>
        <?php echo $pagination?>
        <li class=" <?php if($pageNumber==$totalPage) echo "hidden"?> " ><a href="index.php?pageNumber=<?php echo $pageNumber+1?>">Next</a></li>
    </ul>
    <?php } ?>

    <ul class="pull-right">
        <li> <a href="pdf.php" role="button" class="btn btn-default">Download As PDF</a> </li>
        <li> <a href="xl.php" role="button" class="btn btn-default">Download As Excel</a> </li>
        <li> <a href="multiEmail.php" role="button" class="btn btn-default">Email All</a> </li>
    </ul>
</div>
<script>
    $('#message').show().delay(2000).fadeOut()
</script>

<script type="javascript">
    $( function() {
        var availableTags = [
            <?php echo $comma_separated?>
        ];
        $( "#fTitle" ).autocomplete({
            source: availableTags
        });
    } );
</script>
</body>
</html>
