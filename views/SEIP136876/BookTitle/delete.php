<?php
use App\BITM\SEIP136876\BookTitle\BookTitle;
use App\utils\Utility;
include_once('../../../vendor/autoload.php');
session_start();
$book= new BookTitle();

$book->prepare($_GET);

if($book->delete()){
    $_SESSION['message']="<div class=\"alert alert-success\">
  <strong>Success!</strong> Data successfully deleted
</div>";
    Utility::redirect("index.php");
}
else{
    $_SESSION['message']="<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Failed to delete data
</div>";
    Utility::redirect("index.php");
}
