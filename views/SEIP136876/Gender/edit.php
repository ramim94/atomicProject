<?php
use App\BITM\SEIP136876\Gender\Gender;

include_once('../../../vendor/autoload.php');

$gender= new Gender();
$gender->prepare($_GET);

$singleinfo=$gender->view();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Select Gender</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Selecting gender</h2>

    <form role="form" action="update.php" method="post">
        Enter Your Name: <input type="text" name="username" class="form-control" value="<?php echo $singleinfo['username']?>" /> <br>
        Select Your Gender:
        <label class="radio-inline">
            <input type="radio" name="gender" value="female" <?php if($singleinfo['gender']=="female"){echo "checked";}?> >Female
        </label>
        <label class="radio-inline">
            <input type="radio" name="gender" value="male" <?php if($singleinfo['gender']=="male"){echo "checked";}?> >Male
        </label>
        <label class="radio-inline">
            <input type="radio" name="gender" value="male" <?php if($singleinfo['gender']=="other"){echo "checked";}?> >Other
        </label>
        <br>
        <input type="hidden" name="id" value="<?php echo $singleinfo['id'] ?>" >
        <input type="submit" class="btn btn-info" value="Edit">
    </form>
</div>

</body>
</html>


