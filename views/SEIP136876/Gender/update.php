<?php

use App\utils\Utility;
use App\BITM\SEIP136876\Gender\Gender;
include_once ('../../../vendor/autoload.php');

session_start();


$gender= new Gender();
$gender->prepare($_POST);

if($gender->update()){
    $_SESSION['message']="<div class=\"alert alert-success\">
  <strong>Success!</strong> Data successfully edited
</div>";
    Utility::redirect("index.php");
}
else{
    $_SESSION['message']="<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Failed to edit data
</div>";
    Utility::redirect("index.php");
}
