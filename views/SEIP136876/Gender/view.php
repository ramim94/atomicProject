<?php
use App\BITM\SEIP136876\Gender\Gender;

include_once('../../../vendor/autoload.php');

$gender= new Gender();
$gender->prepare($_GET);

$singleinfo=$gender->view();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Single City View</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>One City:</h2>
    <ul class="list-group">
        <li class="list-group-item"> <strong>User ID: </strong> <?php echo $singleinfo["id"]?> </li>
        <li class="list-group-item"> <strong>User Name: </strong> <?php echo $singleinfo["username"]?> </li>
        <li class="list-group-item"> <strong>User Gender: </strong> <?php echo $singleinfo["gender"]?> </li>

    </ul>

    <a href="index.php" class="btn btn-info" role="button">All Gender</a>
</div>

</body>
</html>
