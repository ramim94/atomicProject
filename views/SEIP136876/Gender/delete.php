<?php

use App\BITM\SEIP136876\Gender\Gender;
use App\utils\Utility;
include_once ('../../../vendor/autoload.php');

$gender= new Gender();

$gender->prepare($_GET);

session_start();

if($gender->delete()){
    $_SESSION['message']="<div class=\"alert alert-success\">
  <strong>Success!</strong> Data successfully Deleted
</div>";
    Utility::redirect("index.php");
}
else{
    $_SESSION['message']="<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Failed to Delete data
</div>";
    Utility::redirect("index.php");
}
