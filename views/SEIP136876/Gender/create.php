<!DOCTYPE html>
<html lang="en">
<head>
    <title>Select Gender</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Selecting gender</h2>

    <form role="form" action="store.php" method="post">
        Enter Your Name: <input type="text" name="username" class="form-control"/> <br>
        Select Your Gender:
        <label class="radio-inline">
            <input type="radio" name="gender" value="female" checked >Female
        </label>
        <label class="radio-inline">
            <input type="radio" name="gender" value="male">Male
        </label>
        <label class="radio-inline">
            <input type="radio" name="gender" value="other">Other
        </label>
        <br>
        <input type="submit" class="btn btn-info" value="Create">
    </form>
</div>

</body>
</html>

