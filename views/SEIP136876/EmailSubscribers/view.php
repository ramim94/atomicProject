<?php
use App\BITM\SEIP136876\EmailSubscribers\EmailSubscribers;

include_once('../../../vendor/autoload.php');

$subscriber= new EmailSubscribers();
$subscriber->prepare($_GET);

$singleuser=$subscriber->view();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Single Email View</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>One Summary</h2>
    <ul class="list-group">
        <li class="list-group-item"> <strong>User ID:</strong> <?php echo $singleuser["id"]?> </li>
        <li class="list-group-item"> <strong>Username:</strong> <?php echo $singleuser["username"]?> </li>
        <li class="list-group-item"> <strong>User Email:</strong> <?php echo $singleuser["email"]?> </li>
        
    </ul>

    <a href="index.php" class="btn btn-info" role="button">All Summaries</a>
</div>

</body>
</html>
