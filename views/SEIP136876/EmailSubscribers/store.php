<?php
use App\BITM\SEIP136876\EmailSubscribers\EmailSubscribers;
use App\utils\Utility;
include_once ('../../../vendor/autoload.php');

$email= new EmailSubscribers();
$email->prepare($_POST);

session_start();

if($email->store()){
    $_SESSION['message']="<div class=\"alert alert-success\">
  <strong>Success!</strong> Data successfully stored
</div>";
    Utility::redirect("index.php");
}
else{
    $_SESSION['message']="<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Failed to store data
</div>";
    Utility::redirect("index.php");
}



//$_SESSION['message']="Data successfully stored";
//
//$email= new EmailSubscribers();
//$email->created($_POST);
//
//Utility::redirect($email->store());