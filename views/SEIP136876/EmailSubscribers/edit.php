<?php
use App\BITM\SEIP136876\EmailSubscribers\EmailSubscribers;
include_once ('../../../vendor/autoload.php');

$subscriber=new EmailSubscribers();
$subscriber->prepare($_GET);
$singleUser=$subscriber->edit();
?>


<html>

<head>
    <title>Update</title>
    <link rel="stylesheet" type="text/css" href="../../../resource/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="../../../resource/js/bootstrap.js"/>
</head>

<!-- Latest compiled and minified CSS
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
-->
<body>
<h3>Title Edit Page</h3>
<form role="form" action="update.php" method="post">

    <div class="form-group">
        <label for="userid" >User id: <?php echo $singleUser["id"] ?> </label><br>
        <label for="username" >User Name: <?php echo $singleUser["username"] ?> </label>
        <input type="hidden" class="form-control" name="id" value=<?php echo $singleUser["id"] ?> />
        <input type="text" class="form-control" name="email"  value=<?php echo $singleUser["email"]; ?> />
    </div>

    <button type="submit" class="btn btn-default">Update</button>
</form>
</body>
</html>
