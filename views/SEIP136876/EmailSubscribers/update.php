<?php
use App\utils\Utility;
include_once ('../../../vendor/autoload.php');
session_start();
$email=new \App\BITM\SEIP136876\EmailSubscribers\EmailSubscribers();

$email->prepare($_POST);

if($email->update()){
    $_SESSION['message']="<div class=\"alert alert-success\">
  <strong>Success!</strong> Data successfully edited
</div>";
    Utility::redirect("index.php");
}
else{
    $_SESSION['message']="<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Failed to edit data
</div>";
    Utility::redirect("index.php");
}

