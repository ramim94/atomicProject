<?php
use App\BITM\SEIP136876\EmailSubscribers\EmailSubscribers;
use App\utils\Utility;
session_start();
include_once ('../../../vendor/autoload.php');
//Utility::dd($_POST);
$users=new EmailSubscribers();

if($users->restoreMultiple($_POST['mark'])){
    $_SESSION['message']="<div class=\"alert alert-success\">
  <strong>Success!</strong> Data successfully restored
</div>";
    Utility::redirect("index.php");
}
else{
    $_SESSION['message']="<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Failed to restore data
</div>";
    Utility::redirect("index.php");
}
