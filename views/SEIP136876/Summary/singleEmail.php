<?php
include_once ('../../../vendor/autoload.php');
include_once ('../../../vendor/phpmailer/phpmailer/class.phpmailer.php');

$book = new \App\BITM\SEIP136876\BookTitle\BookTitle();
$book->prepare($_GET);
$singleBook=$book->view();

//due to array problems trying to get into single value:
$sl=$_GET["sl"];
$oId=$singleBook["id"];
$oTitle=$singleBook["orgname"];
$oSummary=$singleBook["summary"];



//html prepping:(array problem)
$trowdata=" ";
$trowdata.="<tr>";
$trowdata.="<td> $sl </td>";
$trowdata.="<td> $oId</td>";
$trowdata.="<td> $oTitle</td>";
$trowdata.="<td> $oSummary</td>";
$trowdata.="</tr>";


$html= <<<TEMPLATE
    <table>
    <thead>
        <tr>
           <th>sl</th>
        <th>ID</th>
        <th>Organization</th>
        <th>Summary</th>
        </tr>
        </thead>
        <tbody>
            $trowdata
        </tbody>
     </table>
    
TEMPLATE;


#\App\utils\Utility::dd($html);

//mailer goes here
date_default_timezone_set('Etc/UTC');

//Create a new PHPMailer instance
$mail = new PHPMailer;

//Tell PHPMailer to use SMTP
$mail->isSMTP();
$mail->isHTML(true);

//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 2;

//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';

//Set the hostname of the mail server
$mail->Host = 'smtp.gmail.com';
// use
// $mail->Host = gethostbyname('smtp.gmail.com');
// if your network does not support SMTP over IPv6

//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;

//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tls';

//Whether to use SMTP authentication
$mail->SMTPAuth = true;

//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = "bitm.php22@gmail.com";

//Password to use for SMTP authentication
$mail->Password = "bitmphp1234";

//Set who the message is to be sent from
$mail->setFrom('admin@atomicp.com', 'Project Admin');

//Set an alternative reply-to address
$mail->addReplyTo('replyto@example.com', 'First Last');

//Set who the message is to be sent to
$mail->addAddress($singleBook['toemail'], 'Ramim');

//Set the subject line
$mail->Subject = 'PHPMailer GMail SMTP test';

//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
//$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));

//Replace the plain text body with one created manually
//$mail->AltBody = 'This is a plain-text message body';

//Attach an image file
//$mail->addAttachment('images/phpmailer_mini.png');
$mail->Body=$html;


//send the message, check for errors
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {
    echo "Message sent!";
}
