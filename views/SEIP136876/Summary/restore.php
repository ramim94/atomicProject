<?php
use App\Bitm\SEIP136876\Summary\Summary;
use App\utils\Utility;
include_once('../../../vendor/autoload.php');

session_start();

$summary= new Summary();
$summary->prepare($_GET);
if($summary->restore()){
    $_SESSION['message']="<div class=\"alert alert-success\">
  <strong>Success!</strong> Data successfully restored
</div>";
    Utility::redirect("index.php");
}
else{
    $_SESSION['message']="<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Failed to restore data
</div>";
    Utility::redirect("index.php");
}
