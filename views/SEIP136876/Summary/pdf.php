<?php
include_once ("../../../vendor/autoload.php");
include_once ("../../../vendor/mpdf/mpdf/mpdf.php");

$summary=new \App\Bitm\SEIP136876\Summary\Summary();
$alldata=$summary->index();

$trs="";
$sl=0;
foreach($alldata as $data):
    $sl++;
    $trs.="<tr>";
    $trs.="<td> $sl</td>";
    $trs.="<td> $data[id]</td>";
    $trs.="<td> $data[orgname]</td>";
    $trs.="<td> $data[summary]</td>";
    $trs.="</tr>";
endforeach;
$html= <<<BITM

            <table>
                <thead>
                <tr>
                    <th>sl</th>
                    <th>ID</th>
                    <th>Organization title</th>
                    <th>Organization Summary</th>

              </tr>
                </thead>
                <tbody>

                  $trs

                </tbody>
            </table>




BITM;

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output('list.pdf', 'D');