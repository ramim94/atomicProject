<?php
use App\utils\Utility;
use App\Bitm\SEIP136876\Summary\Summary;
include_once ('../../../vendor/autoload.php');
session_start();
$summary=new Summary();

$summary->prepare($_POST);

if($summary->update()){
    $_SESSION['message']="<div class=\"alert alert-success\">
  <strong>Success!</strong> Data successfully edited
</div>";
    Utility::redirect("index.php");
}
else{
    $_SESSION['message']="<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Failed to edit data
</div>";
    Utility::redirect("index.php");
}

