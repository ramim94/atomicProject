<?php
use App\Bitm\SEIP136876\Summary\Summary;

include_once('../../../vendor/autoload.php');

$subscribers= new Summary();
$subscribers->prepare($_GET);

$oneUser=$subscribers->view();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>One Subscriber</h2>
    <ul class="list-group">
        <li class="list-group-item"> <strong>Organization ID:</strong> <?php echo $oneUser["id"]?> </li>
        <li class="list-group-item"> <strong>Organization Name:</strong> <?php echo $oneUser["orgname"]?> </li>
        <li class="list-group-item"> <strong>Organization Summary:</strong> <?php echo $oneUser["summary"]?> </li>
        <li class="list-group-item"> <strong>Will be Emailed to:</strong> <?php echo $oneUser["toemail"]?> </li>

    </ul>

    <a href="index.php" class="btn btn-info" role="button">All Subscribers </a>
</div>

</body>
</html>
