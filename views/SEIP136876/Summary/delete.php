<?php

use App\Bitm\SEIP136876\Summary\Summary;
use App\utils\Utility;
include_once ('../../../vendor/autoload.php');

$summary= new Summary();

$summary->prepare($_GET);

session_start();

if($summary->delete()){
    $_SESSION['message']="<div class=\"alert alert-success\">
  <strong>Success!</strong> Data successfully Deleted
</div>";
    Utility::redirect("index.php");
}
else{
    $_SESSION['message']="<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Failed to Delete data
</div>";
    Utility::redirect("index.php");
}
