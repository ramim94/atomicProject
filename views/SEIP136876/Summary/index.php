<?php
use App\utils\Message;
use App\Bitm\SEIP136876\Summary\Summary;
session_start();
include_once ('../../../vendor/autoload.php');
$summary= new Summary();
//$alluser=$subscribers->index();
$availableCities=$summary->getAllSummary();

$comma_separated= '"'.implode('","',$availableCities).'"';

if(array_key_exists('itemPerPage',$_SESSION)) {
    if (array_key_exists('itemPerPage', $_GET)) {
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
    }
}
else{
    $_SESSION['itemPerPage']=5;
}

$itemPerPage=$_SESSION['itemPerPage'];
$totalItem=$summary->count();

$totalPage=ceil($totalItem/$itemPerPage);
$pagination="";


if(array_key_exists('pageNumber',$_GET)){
    $pageNumber=$_GET['pageNumber'];
}else{
    $pageNumber=1;
}
for($i=1;$i<=$totalPage;$i++){
    $class=($pageNumber==$i)?"active":"";
    $pagination.="<li class='$class'><a href='index.php?pageNumber=$i'>$i</a></li>";
}

$pageStartFrom=$itemPerPage*($pageNumber-1);
$allorg=$summary->paginator($pageStartFrom,$itemPerPage);

if(strtoupper($_SERVER['REQUEST_METHOD'])=='GET'){
    $allorg=$summary->paginator($pageStartFrom,$itemPerPage);

}if(strtoupper($_SERVER['REQUEST_METHOD'])=='POST'){
    $summary->prepare($_POST);
    $allorg=$summary->index();
}
if((strtoupper($_SERVER['REQUEST_METHOD']=='GET'))&& isset($_GET['search'])) {
    $summary->prepare($_GET);
    $allorg = $summary->index();
}




?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Summary List</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>All Summary list</h2>

    <a href="create.php" class="btn btn-info" role="button">Create New</a>
    <a href="trashed.php" class="btn btn-info" role="button">See Trash</a>
    <a href="../../../index.php" class="btn btn-primary" role="button">View Main Page</a>
    <br>
    <br>

    <form method="post" action="index.php">
        <input type="text" class="form-control" id="fTitle" name="filterTitle" placeholder="Filter by Organization Title">
        <p>OR,</p>
        <input type="text" class="form-control" id="fDesc" name="filterDesc" placeholder="Filter by Summary">
        <input type="submit" class="btn-default">
    </form>
    <br>
    <br>

    <form action="index.php" method="get">
        <label>Search: </label>
        <input type="text" name="search" value="">
        <button type="submit">Search</button>
    </form>

    <form role="form">
        <div class="form-group">
            <label for="sel1">Select how many items you want to show (select one):</label>
            <select class="form-control" id="sel1" name="itemPerPage">

                <option <?php if($itemPerPage==5) echo "selected" ?> >5</option>
                <option <?php if($itemPerPage==10) echo "selected" ?> >10</option>
                <option <?php if($itemPerPage==15) echo "selected" ?> >15</option>
                <option <?php if($itemPerPage==20) echo "selected" ?> >20</option>
                <option <?php if($itemPerPage==25) echo "selected" ?> >25</option>
            </select>
            <button type="submit">Go!</button>

        </div>
    </form>


    <table class="table table-striped">
        <thead>
        <tr>
            <th>Sl.</th>
            <th>Id</th>
            <th>Organization Name</th>
            <th>Summary</th>
            <th>Email To</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <div id="message">
            <?php $sl=1;
            echo Message::showMessage();
            foreach ($allorg as $org){ ?>
        </div>
        <tr>
            <td> <?php echo $sl?></td>
            <td> <?php echo $org["id"]?> </td>
            <td><?php echo $org["orgname"]?> </td>
            <td width="200px"><?php echo $org["summary"]?> </td>
            <td><?php echo $org["toemail"]?> </td>
            <td><a href=<?php echo"view.php?id=".$org["id"] ?> role="button" class="btn btn-primary">View</a>
                <a href= <?php echo "edit.php?id=".$org["id"]?> role="button" class="btn btn-success">Edit</a>
                <a href= <?php echo "delete.php?id=".$org["id"]?> role="button" class="btn btn-danger">Delete</a>
                <a href= <?php echo "trash.php?id=".$org["id"]?> role="button" class="btn btn-warning">Trash</a>
                <a href="singleEmail.php?id=<?php echo $org["id"]  ?>&sl=<?php echo $sl+$pageStartFrom ?>" role="button" class="btn btn-primary">Email This</a>
            </td>
        </tr>
        <?php $sl++; } ?>
        </tbody>
    </table>

    <?php if(strtoupper($_SERVER['REQUEST_METHOD']=='GET')) { ?>
    <ul class="pagination">
        <li class=" <?php if($pageNumber==1) echo "hidden"?> "><a href="index.php?pageNumber=<?php echo $pageNumber-1?>">Prev</a></li>
        <?php echo $pagination?>
        <li class=" <?php if($pageNumber==$totalPage) echo "hidden"?> " ><a href="index.php?pageNumber=<?php echo $pageNumber+1?>">Next</a></li>
    </ul>
    <?php } ?>
    
    <ul class="pull-right">
        <li> <a href="pdf.php" role="button" class="btn btn-default">Download As PDF</a> </li>
        <li> <a href="xl.php" role="button" class="btn btn-default">Download As Excel</a> </li>
        <li> <a href="multiEmail.php" role="button" class="btn btn-default">Email All</a> </li>
    </ul>
</div>
<script>
    $('#message').show().delay(2000).fadeOut()
</script>
<script type="javascript">
    $( function() {
        var availableTags = [
            <?php echo $comma_separated?>
        ];
        $( "#fTitle" ).autocomplete({
            source: availableTags
        });
    } );
</script>
</body>
</html>
