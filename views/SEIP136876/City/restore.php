<?php
use App\BITM\SEIP136876\City\City;
use App\utils\Utility;
include_once('../../../vendor/autoload.php');

session_start();

$city= new City();
$city->prepare($_GET);
if($city->restore()){
    $_SESSION['message']="<div class=\"alert alert-success\">
  <strong>Success!</strong> Data successfully restored
</div>";
    Utility::redirect("index.php");
}
else{
    $_SESSION['message']="<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Failed to restore data
</div>";
    Utility::redirect("index.php");
}
