<?php
include_once ("../../../vendor/autoload.php");
include_once ("../../../vendor/mpdf/mpdf/mpdf.php");

$city=new \App\BITM\SEIP136876\City\City();
$alldata=$city->index();

$trs="";
$sl=0;
foreach($alldata as $data):
    $sl++;
    $trs.="<tr>";
    $trs.="<td> $sl</td>";
    $trs.="<td> $data[id]</td>";
    $trs.="<td> $data[username]</td>";
    $trs.="<td> $data[city]</td>";
    $trs.="</tr>";
endforeach;
$html= <<<BITM

            <table>
                <thead>
                <tr>
                    <th>sl</th>
                    <th>ID</th>
                    <th>Username</th>
                    <th>City</th>


              </tr>
                </thead>
                <tbody>

                  $trs

                </tbody>
            </table>




BITM;

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output('list.pdf', 'D');