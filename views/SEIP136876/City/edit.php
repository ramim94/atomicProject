<?php
use App\BITM\SEIP136876\City\City;
include_once ('../../../vendor/autoload.php');
$city=new City();
$city->prepare($_GET);
$onedata=$city->edit();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>City Edit</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Select City</h2>

    <form role="form" action="update.php" method="post">
        <div class="form-group">
            <label>Username:</label>
            <input type="text" name="username" class="form-control" value="<?php echo $onedata["username"]?>" />
            <br>
            <label>Select city (select one):</label>
            <select class="form-control" name="city" >
                <option <?php if($onedata['city']=="Dhaka")echo "selected"?> >Dhaka</option>
                <option <?php if($onedata['city']=="Chittagong")echo "selected"?>  >Chittagong</option>
                <option <?php if($onedata['city']=="Rajshahi")echo "selected"?> >Rajshahi</option>
                <option <?php if($onedata['city']=="Borishal")echo "selected"?> >Borishal</option>
            </select>
            <br>
            <input type="hidden" name="id" value="<?php echo $onedata["id"]?>">
            <input type="submit" role="button" class="btn btn-info">
        </div>
    </form>
</div>

</body>
</html>

