<?php

use App\BITM\SEIP136876\City\City;
use App\utils\Utility;
include_once ('../../../vendor/autoload.php');

$city= new City();

$city->prepare($_GET);

session_start();

if($city->delete()){
    $_SESSION['message']="<div class=\"alert alert-success\">
  <strong>Success!</strong> Data successfully Deleted
</div>";
    Utility::redirect("index.php");
}
else{
    $_SESSION['message']="<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Failed to Delete data
</div>";
    Utility::redirect("index.php");
}
