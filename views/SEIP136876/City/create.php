<!DOCTYPE html>
<html lang="en">
<head>
    <title>City create</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Select City</h2>

    <form role="form" action="store.php" method="post">
        <div class="form-group">
            <label>Enter username:</label>
            <input type="text" name="username" class="form-control">
            <br>
            <label>Select city (select one):</label>

            <select class="form-control" name="city" >
                <option>Dhaka</option>
                <option>Chittagong</option>
                <option>Rajshahi</option>
                <option>Borishal</option>
            </select>
            <br>
            <div class="form-group">
                <label>Friend's Email:</label>
                <input type="email" class="form-control" rows="5" name="email" id="comment">
            </div>
            <input type="submit" role="button" class="btn btn-info">
        </div>

    </form>
</div>

</body>
</html>

