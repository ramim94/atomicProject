<?php

use App\utils\Utility;
use App\BITM\SEIP136876\City\City;
include_once ('../../../vendor/autoload.php');

session_start();

$city= new City();
$city->prepare($_POST);
if($city->store()){
    $_SESSION['message']="<div class=\"alert alert-success\">
  <strong>Success!</strong> Data successfully stored
</div>";
    Utility::redirect("index.php");
}
else{
    $_SESSION['message']="<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Failed to store data
</div>";
    Utility::redirect("index.php");
}
