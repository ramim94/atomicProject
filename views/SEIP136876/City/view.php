<?php
use App\BITM\SEIP136876\City\City;

include_once('../../../vendor/autoload.php');

$city= new City();
$city->prepare($_GET);

$singleinfo=$city->view();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Single City View</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>One City:</h2>
    <ul class="list-group">
        <li class="list-group-item"> <strong>ID:</strong> <?php echo $singleinfo["id"]?> </li>
        <li class="list-group-item"> <strong>Username:</strong> <?php echo $singleinfo["username"]?> </li>
        <li class="list-group-item"> <strong>City:</strong> <?php echo $singleinfo["city"]?> </li>
        <li class="list-group-item"> <strong>Will be Emailed to:</strong> <?php echo $singleinfo["toemail"]?> </li>

    </ul>

    <a href="index.php" class="btn btn-info" role="button">All City</a>
</div>

</body>
</html>
