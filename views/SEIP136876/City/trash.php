<?php

use App\BITM\SEIP136876\City\City;
use App\utils\Utility;
include_once('../../../vendor/autoload.php');
session_start();
$summary= new City();

$summary->prepare($_GET);

if($summary->trash()){
    $_SESSION['message']="<div class=\"alert alert-success\">
  <strong>Success!</strong> Data successfully trashed
</div>";
    Utility::redirect("index.php");
}
else{
    $_SESSION['message']="<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Failed to trash data
</div>";
    Utility::redirect("index.php");
}
