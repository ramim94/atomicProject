<?php

use App\BITM\SEIP136876\Birthday\Birthday;

include_once ('../../../vendor/autoload.php');

$user=new Birthday();
?>

<html>

<head>
    <title>Birthday Create</title>
    <link rel="stylesheet" type="text/css" href="../../../resource/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="../../../resource/js/bootstrap.js"/>
</head>

<!-- Latest compiled and minified CSS
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
-->
<h3>Birthday Entry</h3>

<form role="form" action="store.php" method="post">

    <div class="form-group">
        <label>Enter the username below:</label>
        <input type="text" class="form-control" name="username">
        <label>Enter your birthday below:</label>
        <input type="text" class="form-control" name="birthday" placeholder="DD-MM-YYYY">
    </div>

    <button type="submit" class="btn btn-default">Submit</button>
</form>

</html>
