<?php
session_start();
use App\utils\Message;
use App\BITM\SEIP136876\Birthday\Birthday;

include_once("../../../vendor/autoload.php");
$birthday= new Birthday();
$userinfos=$birthday->trashed();
//Utility::dd($bookinfos);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>All trash</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">

    <h2>Trashed Birthdays</h2>
    <p>
        <a href="index.php" role="button" class="btn btn-info" type="button">See All Birthdays</a>
    </p>

    <form action="multipleSelect.php" method="post" id="multiple">

        <input type="submit" class="btn btn-info" name="restore" value="Restore Selected">
        <input type="submit" class="btn btn-info" name="delete" value="Delete Button">


        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Check</th>
                <th>Sl.</th>
                <th>Id</th>
                <th>User Name</th>
                <th>Birthday</th>
                <th>Actions</th>
            </tr>
            </thead>

            <tbody>
            <div id="message">
                <?php $sl=1;
                echo Message::showMessage();
                foreach ($userinfos as $user){ ?>
            </div>
            <tr>
                <td><input type="checkbox" name="mark[]" value="<?php echo $user["id"]?>"></td>
                <td> <?php echo $sl?></td>
                <td> <?php echo $user["id"]?> </td>
                <td><?php echo $user["username"]?> </td>
                <td><?php echo $user["birthday"]?> </td>
                <td><a href="restore.php?id=<?php echo $user["id"]  ?>" class="btn btn-primary" role="button">Restore</a>
                    <a href="delete.php?id=<?php echo $user["id"]  ?>" role="button" class="btn btn-danger" id="delete">Delete</a>
                </td>
            </tr>
            <?php $sl++; } ?>
            </tbody>
        </table>
    </form>
</div>

<script>


    $('#multiDelete').on('click',function(){
        document.forms[0].action="deletemultiple.php";
        $('#multiple').submit();
    });

</script>
</body>
</html>

