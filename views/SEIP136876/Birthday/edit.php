<?php
use App\BITM\SEIP136876\Birthday\Birthday;

include_once('../../../vendor/autoload.php');

$birthday= new Birthday();
$birthday->prepare($_GET);

$singleinfo=$birthday->view();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Select Gender</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Edit Birthday</h2>

    <form role="form" action="update.php" method="post">
        <label>Enter the username below:</label>
        <input type="text" class="form-control" name="username" value="<?php echo $singleinfo['username']?>">
        <label>Enter your birthday below:</label>
        <input type="text" class="form-control" name="birthday" value="<?php $day=explode('-',$singleinfo['birthday']); echo $day[2]."-".$day[1]."-".$day[0]; ?> ">
        <br>
        <input type="hidden" name="id" value="<?php echo $singleinfo['id'] ?>" >
        <input type="submit" class="btn btn-info" value="Edit">
    </form>
</div>

</body>
</html>


