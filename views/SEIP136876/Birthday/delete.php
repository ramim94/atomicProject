<?php

use App\BITM\SEIP136876\Birthday\Birthday;
use App\utils\Utility;
include_once ('../../../vendor/autoload.php');

$birthday= new Birthday();

$birthday->prepare($_GET);

session_start();

if($birthday->delete()){
    $_SESSION['message']="<div class=\"alert alert-success\">
  <strong>Success!</strong> Data successfully Deleted
</div>";
    Utility::redirect("index.php");
}
else{
    $_SESSION['message']="<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Failed to Delete data
</div>";
    Utility::redirect("index.php");
}

