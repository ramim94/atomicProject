<?php
use App\BITM\SEIP136876\Birthday\Birthday;

include_once('../../../vendor/autoload.php');

$birthday= new Birthday();
$birthday->prepare($_GET);

$singleinfo=$birthday->view();
$day=explode('-',$singleinfo['birthday']);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Single Birthday View</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Single User Birthday:</h2>
    <ul class="list-group">
        <li class="list-group-item"> <strong>User ID: </strong> <?php echo $singleinfo["id"]?> </li>
        <li class="list-group-item"> <strong>User Name: </strong> <?php echo $singleinfo["username"]?> </li>
        <li class="list-group-item"> <strong>User Birthday: </strong> <?php echo $day[2]."-".$day[1]."-".$day[0]?> </li>

    </ul>

    <a href="index.php" class="btn btn-info" role="button">All Birthdays</a>
</div>

</body>
</html>
