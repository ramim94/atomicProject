<?php
use App\utils\Utility;
use App\BITM\SEIP136876\Birthday\Birthday;
include_once ('../../../vendor/autoload.php');
session_start();
$birthday=new Birthday();

$day=explode("-", $_POST['birthday']);
$_POST['birthday']=$day[2]."-".$day[1]."-".$day[0];

$birthday->prepare($_POST);
if($birthday->store()){
    $_SESSION['message']="<div class=\"alert alert-success\">
  <strong>Success!</strong> Data successfully stored
</div>";
    Utility::redirect("index.php");
}
else{
    $_SESSION['message']="<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Failed to store data
</div>";
    Utility::redirect("index.php");
}
