<?php
use App\BITM\SEIP136876\Hobby\Hobby;
use App\utils\Utility;
include_once ('../../../vendor/autoload.php');
session_start();
//Utility::dd($_POST);

$users=new Hobby();

if($users->deleteMultiple($_POST['mark'])){
    $_SESSION['message']="<div class=\"alert alert-success\">
  <strong>Success!</strong> Data successfully deleted
</div>";
    Utility::redirect("index.php");
}
else{
    $_SESSION['message']="<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Failed to delete data
</div>";
    Utility::redirect("index.php");
}
