<?php
use App\utils\Utility;
use App\BITM\SEIP136876\Hobby\Hobby;
include_once('../../../vendor/autoload.php');

//Utility::dd($_POST);

$hobby= new Hobby();
session_start();
$comm_separated=implode(",",$_POST['hobby']);
$_POST['hobby']=$comm_separated;
//Utility::dd($_POST);
$hobby->prepare($_POST);
if($hobby->store()){
    $_SESSION['message']="<div class=\"alert alert-success\">
  <strong>Success!</strong> Data successfully stored
</div>";
    Utility::redirect("index.php");
}
else{
    $_SESSION['message']="<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Failed to store data
</div>";
    Utility::redirect("index.php");
}