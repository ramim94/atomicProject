<?php

use App\BITM\SEIP136876\Hobby\Hobby;
use App\utils\Utility;

include_once('../../../vendor/autoload.php');

$hobby=new Hobby();
$hobby->prepare($_GET);
$allhobbies=$hobby->edit();


//Utility::dd($foundArrays);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Hobby Edit</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Hobby Edit</h2>


    <form role="form" action="update.php" method="post">

            <label><input type="hidden" name="id" value="<?php echo $_GET['id']?>"> </label>

        <div class="checkbox">

            <label><input type="checkbox" name="hobby[]" value="Cricket" <?php  if (in_array("Cricket",$allhobbies)) echo "checked"?> >Cricket</label>

        </div>
        <div class="checkbox">

            <label><input type="checkbox" name="hobby[]" value="Gardening" <?php  if (in_array("Gardening",$allhobbies)) echo "checked"?> >Gardening</label>

        </div>
        <div class="checkbox">

            <label><input type="checkbox" name="hobby[]" value="Football" <?php  if (in_array("Football",$allhobbies)) echo "checked"?> >Football</label>

        </div>
        <div class="checkbox">

            <label><input type="checkbox" name="hobby[]" value="Coding" <?php  if (in_array("Coding",$allhobbies)) echo "checked"?> >Coding</label>

        </div>
        <div class="checkbox">

            <label><input type="checkbox" name="hobby[]" value="Music" <?php  if (in_array("Music",$allhobbies)) echo "checked"?> >Music</label>

        </div>
        <input type="submit" class="btn-info">
    </form>
</div>

</body>
</html>