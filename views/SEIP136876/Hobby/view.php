<?php
include_once('../../../vendor/autoload.php');
use App\BITM\SEIP136876\Hobby\Hobby;
use App\utils\Utility;
$hobby=new Hobby();
$hobby->prepare($_GET);
$oneUser=$hobby->view();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Single Hobby</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Single User Hobby</h2>
    <ul class="list-group">
        <li class="list-group-item"> <strong>ID:</strong> <?php echo $oneUser["id"]?> </li>
        <li class="list-group-item"> <strong>Hobbies:</strong> <?php echo $oneUser["hobbies"]?> </li>

    </ul>
    <a href="index.php" class="btn btn-info" role="button">All Hobbies</a>
</div>

</body>
</html>



