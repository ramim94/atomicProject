<!DOCTYPE html>
<html lang="en">
<head>
    <title>New Hobby</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Hobby Selection</h2>


    <form role="form" action="store.php" method="post">
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Cricket">Cricket</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Gardening">Gardening</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Football">Football</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Coding">Coding</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Music">Music</label>
        </div>
    <input type="submit" class="btn-info">
    </form>
</div>

</body>
</html>

