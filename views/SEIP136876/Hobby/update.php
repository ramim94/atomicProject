<?php
session_start();
use App\utils\Utility;
use App\BITM\SEIP136876\Hobby\Hobby;
include_once('../../../vendor/autoload.php');


//Utility::dd($_POST);

$hobby=new Hobby();
$stringHobbies=implode(",",$_POST['hobby']);
$_POST['hobby']=$stringHobbies;

$hobby->prepare($_POST);
if($hobby->update()){
    $_SESSION['message']="<div class=\"alert alert-success\">
  <strong>Success!</strong> Data successfully edited
</div>";
    Utility::redirect("index.php");
}
else{
    $_SESSION['message']="<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Failed to edit data
</div>";
    Utility::redirect("index.php");
}