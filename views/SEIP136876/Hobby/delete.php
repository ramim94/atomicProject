<?php

use App\BITM\SEIP136876\Hobby\Hobby;
use App\utils\Utility;
include_once ('../../../vendor/autoload.php');

$hobby= new Hobby();

$hobby->prepare($_GET);

session_start();

if($hobby->delete()){
    $_SESSION['message']="<div class=\"alert alert-success\">
  <strong>Success!</strong> Data successfully Deleted
</div>";
    Utility::redirect("index.php");
}
else{
    $_SESSION['message']="<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Failed to Delete data
</div>";
    Utility::redirect("index.php");
}
