<?php
session_start();

use App\utils\Message;
use App\Bitm\SEIP136876\ProfilePicture\ProfilePicture;

include_once("../../../vendor/autoload.php");
$alldp= new ProfilePicture();
$dps=$alldp->trashed();
//Utility::dd($bookinfos);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Trashed Pictures</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">

    <h2>Trashed Hobbies</h2>
    <p>
        <a href="index.php" role="button" class="btn btn-info" type="button">See All Pictures</a>
    </p>

    <form action="multipleSelect.php" method="post" id="multiple">
        <input type="submit" class="btn btn-info" name="restore" value="Restore Multiple">
        <input type="submit" class="btn btn-info" name="delete" value="Delete Multiple">


<!--        <button  class="btn btn-info" type="button" id="multiDelete">Delete selected</button>-->


        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Check</th>
                <th>Sl.</th>
                <th>Id</th>
                <th>User Name</th>
                <th>Image</th>
                <th>Actions</th>
            </tr>
            </thead>

            <tbody>
            <div id="message">
                <?php $sl=1;
                echo Message::showMessage();
                foreach ($dps as $info){ ?>
            </div>
            <tr>
                <td><input type="checkbox" name="mark[]" value="<?php echo $info["id"]?>"></td>
                <td> <?php echo $sl?></td>
                <td> <?php echo $info["id"]?> </td>
                <td><?php echo $info["name"]?> </td>
                <td> <img src="../../../resource/Images/<?php echo $info["images"]?>" alt="image" width="100px" height="100px"/>  </td>
                <td><a href="restore.php?id=<?php echo $info["id"]  ?>" class="btn btn-primary" role="button">Restore</a>
                    <a href="delete.php?id=<?php echo $info["id"]  ?>" role="button" class="btn btn-danger" id="delete">Delete</a>
                </td>
            </tr>
            <?php $sl++; } ?>
            </tbody>
        </table>
    </form>
</div>

<script>


    $('#multiDelete').on('click',function(){
        document.forms[0].action="deletemultiple.php";
        $('#multiple').submit();
    });

</script>
</body>
</html>

