<?php
use App\Bitm\SEIP136876\ProfilePicture\ProfilePicture;
use App\utils\Utility;
include_once('../../../vendor/autoload.php');

$dp=new ProfilePicture();
session_start();

if(isset($_FILES)&& !empty($_FILES['images']['name'])){
    $filename= time().$_FILES['images']['name'];
    $location=$_FILES['images']['tmp_name'];
    move_uploaded_file($location,$_SERVER['DOCUMENT_ROOT']."/atomicProject/resource/Images/".$filename); //home
//BITM    move_uploaded_file($location,$_SERVER['DOCUMENT_ROOT']."/AtomicProjectB22_ramim_136876/resource/Images/".$filename);
    $_POST['images']=$filename;

    $dp->prepare($_POST);

    if($dp->store()){
        $_SESSION['message']="<div class=\"alert alert-success\">
  <strong>Success!</strong> Data successfully stored
</div>";
        Utility::redirect("index.php");
    }
    else{
        $_SESSION['message']="<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Failed to store data
</div>";
        Utility::redirect("index.php");
    }


}
