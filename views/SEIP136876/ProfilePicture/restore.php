<?php
use App\Bitm\SEIP136876\ProfilePicture\ProfilePicture;
use App\utils\Utility;
include_once('../../../vendor/autoload.php');

session_start();

$dp= new ProfilePicture();
$dp->prepare($_GET);
if($dp->restore()){
    $_SESSION['message']="<div class=\"alert alert-success\">
  <strong>Success!</strong> Data successfully restored
</div>";
    Utility::redirect("index.php");
}
else{
    $_SESSION['message']="<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Failed to restore data
</div>";
    Utility::redirect("index.php");
}
