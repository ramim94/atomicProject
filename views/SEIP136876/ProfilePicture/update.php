<?php
session_start();

use App\utils\Utility;
use App\Bitm\SEIP136876\ProfilePicture\ProfilePicture;
include_once('../../../vendor/autoload.php');


//Utility::dd($_POST);

session_start();
$dp=new ProfilePicture();
$dp->prepare($_POST);

if(isset($_FILES) && !empty($_FILES['images']['name'])){
    $oldfile=$dp->view();
    Utility::nlink($oldfile['images']); //change root folder for BITMPC access in utility
    
    $filename= time().$_FILES['images']['name'];
    $location=$_FILES['images']['tmp_name'];
    move_uploaded_file($location,$_SERVER['DOCUMENT_ROOT']."/atomicProject/resource/Images/".$filename); //home
    $_POST['images']=$filename;
    $dp->prepare($_POST);

    if($dp->update()){
        $_SESSION['message']="<div class=\"alert alert-success\">
  <strong>Success!</strong> Data successfully stored
</div>";
        Utility::redirect("index.php");
    }
    else{
        $_SESSION['message']="<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Failed to store data
</div>";
        Utility::redirect("index.php");
    }


}else{
    if($dp->update()){
        $_SESSION['message']="<div class=\"alert alert-success\">
  <strong>Success!</strong> Data successfully stored
</div>";
        Utility::redirect("index.php");
    }
    else{
        $_SESSION['message']="<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Failed to store data
</div>";
        Utility::redirect("index.php");
    }


}