<?php
use App\Bitm\SEIP136876\ProfilePicture\ProfilePicture;
use App\utils\Utility;
include_once ('../../../vendor/autoload.php');

session_start();
$dp=new ProfilePicture();
$dp->prepare($_GET);
$info=$dp->view();
Utility::nlink($info['images']);

$dp->prepare($info);
if($dp->delete()){
    $_SESSION['message']="<div class=\"alert alert-success\">
  <strong>Success!</strong> Data successfully deleted
</div>";
    Utility::redirect("index.php");
}
else{
    $_SESSION['message']="<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Failed to delete data
</div>";
    Utility::redirect("index.php");
}
