<?php
use App\Bitm\SEIP136876\ProfilePicture\ProfilePicture;

include_once('../../../vendor/autoload.php');

$dp= new ProfilePicture();
$dp->prepare($_GET);

$singleInfo=$dp->view();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Information</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Single Information View</h2>
    <ul class="list-group">
        <li class="list-group-item"> <strong>User ID:</strong> <?php echo $singleInfo["id"]?> </li>
        <li class="list-group-item"> <strong>User Name:</strong> <?php echo $singleInfo["name"]?> </li>
        <li class="list-group-item"> <strong>Image:</strong> <img src="../../../resource/Images/<?php echo $singleInfo["images"]?>" /> </li>
    </ul>
</div>
<a href="index.php" class="btn btn-info" role="button">All Profile Pictures</a>
</body>
</html>


