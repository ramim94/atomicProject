<?php
session_start();
use App\utils\Utility;
use App\utils\Message;
use App\Bitm\SEIP136876\ProfilePicture\ProfilePicture;
include_once("../../../vendor/autoload.php");
$dp=new ProfilePicture();
//$piclist=$dp->index();
//Utility::dd($userhobby);

if(array_key_exists('itemPerPage',$_SESSION)) {
    if (array_key_exists('itemPerPage', $_GET)) {
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
    }
}
else{
    $_SESSION['itemPerPage']=5;
}

$itemPerPage=$_SESSION['itemPerPage'];
$totalItem=$dp->count();

$totalPage=ceil($totalItem/$itemPerPage);
$pagination="";


if(array_key_exists('pageNumber',$_GET)){
    $pageNumber=$_GET['pageNumber'];
}else{
    $pageNumber=1;
}
for($i=1;$i<=$totalPage;$i++){
    $class=($pageNumber==$i)?"active":"";
    $pagination.="<li class='$class'><a href='index.php?pageNumber=$i'>$i</a></li>";
}

$pageStartFrom=$itemPerPage*($pageNumber-1);
$piclist=$dp->paginator($pageStartFrom,$itemPerPage);

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>User Picture List</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">

    <h2>All ProfilePicture list</h2>


    <a href="create.php" class="btn btn-info" role="button">Create New </a>
    <a href="trashed.php" class="btn btn-info" role="button">See Trash </a>
    <a href="../../../index.php" class="btn btn-primary" role="button">View Main Page</a>
    <form role="form">
        <div class="form-group">
            <label for="sel1">Select how many items you want to show (select one):</label>
            <select class="form-control" id="sel1" name="itemPerPage">

                <option <?php if($itemPerPage==5) echo "selected" ?> >5</option>
                <option <?php if($itemPerPage==10) echo "selected" ?> >10</option>
                <option <?php if($itemPerPage==15) echo "selected" ?> >15</option>
                <option <?php if($itemPerPage==20) echo "selected" ?> >20</option>
                <option <?php if($itemPerPage==25) echo "selected" ?> >25</option>
            </select>
            <button type="submit">Go!</button>

        </div>
    </form>
    <table  class="table table-bordered">
        <thead>
        <tr>
            <th>Sl.</th>
            <th>Id</th>
            <th>User Name</th>
            <th>User Picture</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <div id="message">
            <?php $sl=1;
            echo Message::showMessage();
            foreach ($piclist as $eachinfo){ ?>
        </div>
        <tr>
            <td> <?php echo $sl?></td>
            <td> <?php echo $eachinfo["id"]?> </td>
            <td><?php echo $eachinfo["name"]?> </td>
            <td> <img src="../../../resource/Images/<?php echo $eachinfo["images"]?>" alt="image" width="100px" height="100px"/>  </td>
            <td><a href="view.php?id=<?php echo $eachinfo["id"]  ?>" class="btn btn-primary" role="button">View</a>
                <a href= <?php echo "edit.php?id=".$eachinfo["id"]?> role="button" class="btn btn-success">Edit</a>
                <a href="delete.php?id=<?php echo $eachinfo["id"]  ?>" role="button" class="btn btn-danger">Delete</a>
                <a href="trash.php?id=<?php echo $eachinfo["id"]  ?>" role="button" class="btn btn-warning">Trash</a>
            </td>
        </tr>
        <?php $sl++; } ?>
        </tbody>
    </table>

    <ul class="pagination">
        <li class=" <?php if($pageNumber==1) echo "hidden"?> "><a href="index.php?pageNumber=<?php echo $pageNumber-1?>">Prev</a></li>
        <?php echo $pagination?>
        <li class=" <?php if($pageNumber==$totalPage) echo "hidden"?> " ><a href="index.php?pageNumber=<?php echo $pageNumber+1?>">Next</a></li>
    </ul>
</div>
<script>
    $('#message').show().delay(2000).fadeOut()
</script>

</body>
</html>
