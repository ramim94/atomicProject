<!DOCTYPE html>
<html lang="en">
<head>
    <title>New Profile Picture</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>New Profile Picture</h2>

    <form role="form" action="store.php" enctype="multipart/form-data" method="post">
        <div class="form-group">
            <label >User Name:</label>
            <input type="text" class="form-control" name="name" placeholder="Enter User Name">
        </div>
        <div class="form-group">
            <label for="pwd">Image:</label>
            <input type="file" class="form-control" name="images" >
        </div>
        <input type="submit" value="Submit">
    </form>
</div>

</body>
</html>

