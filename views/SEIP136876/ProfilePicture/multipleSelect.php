<?php
use App\utils\Utility;
use App\Bitm\SEIP136876\ProfilePicture\ProfilePicture;
include_once ('../../../vendor/autoload.php');
session_start();

if(array_key_exists("restore",$_POST)){
    $users=new ProfilePicture();

    if($users->restoreMultiple($_POST['mark'])){
        $_SESSION['message']="<div class=\"alert alert-success\">
  <strong>Success!</strong> Data successfully restored
</div>";
        Utility::redirect("index.php");
    }
    else{
        $_SESSION['message']="<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Failed to restore data
</div>";
        Utility::redirect("index.php");
    }

}else if(array_key_exists("delete", $_POST)){
    $users=new ProfilePicture();

    $total=count($_POST['mark']);
    for($i=0;$i<$total;$i++){
        $users=new ProfilePicture();
        $users->setID($_POST['mark'][$i]);
        $singleUser=$users->view();
        Utility::nlink($singleUser['images']);
    }


    if($users->deleteMultiple($_POST['mark'])){
        $_SESSION['message']="<div class=\"alert alert-success\">
  <strong>Success!</strong> Data successfully deleted
</div>";
        Utility::redirect("index.php");
    }
    else{
        $_SESSION['message']="<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Failed to delete data
</div>";
        Utility::redirect("index.php");
    }
}
