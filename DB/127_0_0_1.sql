-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 14, 2016 at 03:49 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomicprojectb22`
--
CREATE DATABASE IF NOT EXISTS `atomicprojectb22` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `atomicprojectb22`;

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

DROP TABLE IF EXISTS `birthday`;
CREATE TABLE IF NOT EXISTS `birthday` (
`id` int(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `birthday` date NOT NULL,
  `deleted_at` int(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `birthday`
--

TRUNCATE TABLE `birthday`;
--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `username`, `birthday`, `deleted_at`) VALUES
(1, 'first', '2016-06-09', NULL),
(4, 'ramim', '1994-10-24', NULL),
(5, 'nw', '1992-07-19', 1468389832),
(6, 'ramim2', '1993-10-20', 1468389830);

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
CREATE TABLE IF NOT EXISTS `book` (
`id` int(50) NOT NULL,
  `title` varchar(100) NOT NULL,
  `toemail` varchar(255) NOT NULL DEFAULT 'm.ramim94@gmail.com',
  `deleted_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `book`
--

TRUNCATE TABLE `book`;
--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `title`, `toemail`, `deleted_at`) VALUES
(3, 'newest Book', 'm.ramim94@gmail.com', NULL),
(4, 'Guliver_Travels', 'm.ramim94@gmail.com', NULL),
(5, 'Castle', 'm.ramim94@gmail.com', NULL),
(6, 'new era', 'm.ramim94@gmail.com', NULL),
(7, 'blehyahoo', 'm.ramim94@gmail.com', NULL),
(8, 'o-level', 'm.ramim94@gmail.com', NULL),
(9, 'alevel', 'm.ramim94@gmail.com', NULL),
(10, 'done', 'm.ramim94@gmail.com', NULL),
(11, 'another', 'm.ramim94@gmail.com', NULL),
(12, 'none', 'm.ramim94@gmail.com', NULL),
(13, 'well', 'm.ramim94@gmail.com', NULL),
(14, 'ok', 'm.ramim94@gmail.com', NULL),
(15, 'one space', 'm.ramim94@gmail.com', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
CREATE TABLE IF NOT EXISTS `city` (
`id` int(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `deleted_at` int(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `city`
--

TRUNCATE TABLE `city`;
--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `username`, `city`, `deleted_at`) VALUES
(1, 'ramim', 'Chittagong', 1467275720),
(3, 'ramim', 'Chittagong', NULL),
(4, 'oita', 'Dhaka', NULL),
(5, 'newuser', 'Borishal', 1467275724),
(6, 'Email address', 'Rajshahi', NULL),
(7, 'new', 'Rajshahi', NULL),
(8, 'sdfasdf', 'Dhaka', NULL),
(9, 'sadfsadf', 'Chittagong', NULL),
(10, 'fdfdsafs', 'Rajshahi', NULL),
(11, 'ffdsafsadfsadfas', 'Borishal', NULL),
(12, 'fdsadfsadfas', 'Rajshahi', NULL),
(13, 'dsfadsfasdf', 'Chittagong', NULL),
(14, 'fdsafdsfas', 'Borishal', NULL),
(15, 'dsfasdfa', 'Chittagong', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

DROP TABLE IF EXISTS `email`;
CREATE TABLE IF NOT EXISTS `email` (
`id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `deleted_at` int(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `email`
--

TRUNCATE TABLE `email`;
--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `username`, `email`, `deleted_at`) VALUES
(3, 'newuser', 'testuser1@gmail.com', 1467275704),
(6, 'user2', 'user2@tmail.com', 1467275705),
(8, 'beenuser', 'beenuser@gmail.com', NULL),
(9, 'anyuser', 'suerkj@gamim', NULL),
(10, 'dsfadsf', 'dsfafd@asdfas', NULL),
(11, 'fdsfasa', 'fdahfdg@hdgfh', NULL),
(12, 'sdfffdgsg', 'sadffdsgdgfr@kjhgkjh', NULL),
(13, 'hjkgghtyj', 'sfdgdg@fdgsffdg', NULL),
(14, 'hfjdgj', 'jhdrgtd@asdfg', NULL),
(15, 'asdfds', 'jgfjuhighv@hfgd', NULL),
(16, 'dsafdhgfujf', 'fdgsdf@garfd', NULL),
(17, 'jhiktyerst', 'dsfery@gfadg', NULL),
(18, 'bvnfghd', 'sdfvb@jhtfyyh', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

DROP TABLE IF EXISTS `gender`;
CREATE TABLE IF NOT EXISTS `gender` (
`id` int(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `deleted_at` int(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `gender`
--

TRUNCATE TABLE `gender`;
--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `username`, `gender`, `deleted_at`) VALUES
(1, 'ramim', 'male', NULL),
(2, 'ramim', 'male', NULL),
(5, 'ramim', 'male', NULL),
(6, 'Email address', 'female', NULL),
(8, 'new', 'male', NULL),
(9, 'asdfasdf', 'other', NULL),
(10, 'dsgfsg', 'male', NULL),
(11, 'dsfsdf', 'female', NULL),
(12, 'fsdfsdf', 'other', NULL),
(13, 'dsfsfdvdxcv', 'other', NULL),
(14, 'vxcvsdf', 'male', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hobby`
--

DROP TABLE IF EXISTS `hobby`;
CREATE TABLE IF NOT EXISTS `hobby` (
`id` int(255) NOT NULL,
  `hobbies` varchar(255) NOT NULL,
  `deleted_at` int(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `hobby`
--

TRUNCATE TABLE `hobby`;
--
-- Dumping data for table `hobby`
--

INSERT INTO `hobby` (`id`, `hobbies`, `deleted_at`) VALUES
(2, 'Gardening,Football,Coding', 1466272128),
(3, 'Cricket,Coding,Music', 1466272125),
(4, 'Coding', NULL),
(7, 'Football,Coding,Music', NULL),
(8, 'Gardening,Football,Coding', NULL),
(9, 'Cricket,Gardening,Coding', NULL),
(10, 'Cricket,Gardening,Coding', NULL),
(11, 'Gardening,Football,Coding', NULL),
(12, 'Gardening,Football,Coding', NULL),
(13, 'Cricket,Football,Music', NULL),
(14, 'Gardening,Football,Music', NULL),
(15, 'Cricket,Football,Coding', NULL),
(16, 'Cricket,Gardening,Football', NULL),
(17, 'Gardening,Football,Coding', NULL),
(18, 'Cricket,Football,Music', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profilepicture`
--

DROP TABLE IF EXISTS `profilepicture`;
CREATE TABLE IF NOT EXISTS `profilepicture` (
`id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `images` varchar(255) NOT NULL,
  `deleted_at` int(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `profilepicture`
--

TRUNCATE TABLE `profilepicture`;
--
-- Dumping data for table `profilepicture`
--

INSERT INTO `profilepicture` (`id`, `name`, `images`, `deleted_at`) VALUES
(17, 'sevilla', '14672686393.jpg', NULL),
(18, 'new', '14672685931.jpg', NULL),
(21, 'dsafas', '14672770844.jpg', NULL),
(22, 'fddsf', '14672770923.jpg', NULL),
(23, 'dsfgdsfg', '14672771742.jpg', NULL),
(24, 'gffdhsdh', '14672771804.jpg', NULL),
(25, 'new', '14672774903.jpg', 1468389674);

-- --------------------------------------------------------

--
-- Table structure for table `summary`
--

DROP TABLE IF EXISTS `summary`;
CREATE TABLE IF NOT EXISTS `summary` (
`id` int(255) NOT NULL,
  `orgname` varchar(255) NOT NULL,
  `summary` varchar(255) NOT NULL,
  `deleted_at` int(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `summary`
--

TRUNCATE TABLE `summary`;
--
-- Dumping data for table `summary`
--

INSERT INTO `summary` (`id`, `orgname`, `summary`, `deleted_at`) VALUES
(1, 'neworganization', 'presitigious', NULL),
(3, 'asdf', '   new proper summary 2', NULL),
(4, 'asdfasdfasfd', 'asdfasdfas', NULL),
(5, 'fsdafdsfasdfasfd', 'asdfadf', NULL),
(6, 'asdfasdfadf', 'sadfasfasf', 1468390984),
(7, 'sadfasdf', 'adsfasdf', 1468390987),
(8, 'dsfsdfdsaf', 'dsfadsfasdfasdfadsfasdfsadf sadfsa dfsad fasdfasdfasdfasd fasfd sad fadsfasdfasdfasdfa', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book`
--
ALTER TABLE `book`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobby`
--
ALTER TABLE `hobby`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profilepicture`
--
ALTER TABLE `profilepicture`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary`
--
ALTER TABLE `summary`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
MODIFY `id` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `hobby`
--
ALTER TABLE `hobby`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `profilepicture`
--
ALTER TABLE `profilepicture`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `summary`
--
ALTER TABLE `summary`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
