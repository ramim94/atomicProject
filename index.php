<?php
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Main Page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <h2>Link to All pages</h2>


<ul class="list-group">
    <li class="list-group-item"> <label> VISIT BIRTHDAY: <a href="views/SEIP136876/Birthday/index.php" type="button" class="btn btn-default">GO!</a></label></li>
    <li class="list-group-item"> <label> VISIT BOOKTITLE: <a href="views/SEIP136876/BookTitle/index.php" type="button" class="btn btn-primary">GO!</a></label> </li>
    <li class="list-group-item"> <label> VISIT CITY: <a href="views/SEIP136876/City/index.php" type="button" class="btn btn-default">GO!</a></label> </li>
    <li class="list-group-item"> <label> VISIT EMAIL: <a href="views/SEIP136876/EmailSubscribers/index.php" type="button" class="btn btn-primary">GO!</a></label> </li>
    <li class="list-group-item"> <label> VISIT GENDER:<a href="views/SEIP136876/Gender/index.php" type="button" class="btn btn-default">GO!</a></label> </li>
    <li class="list-group-item"> <label> VISIT HOBBY:<a href="views/SEIP136876/Hobby/index.php" type="button" class="btn btn-primary">GO!</a></label> </li>
    <li class="list-group-item"> <label> VISIT PROFILE PICTURE:<a href="views/SEIP136876/ProfilePicture/index.php" type="button" class="btn btn-default">GO!</a></label> </li>
    <li class="list-group-item"> <label> VISIT SUMMARY: <a href="views/SEIP136876/Summary/index.php" type="button" class="btn btn-primary">GO!</a></label> </li>
</ul>
</body>
</html>

