<?php
namespace App\BITM\SEIP136876\Birthday;


class Birthday{
    public $id;
    public $username;
    public $birthday;
    public $conn;

    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","atomicprojectb22") or die("Database Connection Error");
    }
    public function prepare($arraydata){
        if(array_key_exists("username",$arraydata)){
            $this->username=$arraydata['username'];
        }if(array_key_exists("id",$arraydata)){
            $this->id=$arraydata['id'];
        }if(array_key_exists("birthday",$arraydata)){
            $this->birthday=$arraydata['birthday'];
        }
    }
    
    public function store(){
        $query="INSERT INTO `birthday` (`username`, `birthday`) VALUES ('".$this->username."', '".$this->birthday."')";
        if(mysqli_query($this->conn, $query)){
            return true;
        }else return false;
    }   
    
    public function count(){
    $query="SELECT COUNT(*) AS totalItem FROM `atomicprojectb22`.`birthday` WHERE `deleted_at` is null";
    $result=mysqli_query($this->conn,$query);
    $row= mysqli_fetch_assoc($result);
    return $row['totalItem'];
}
    public function paginator($pageStartFrom=0,$Limit=5){
        $_allBook = array();
        $query="SELECT * FROM `birthday` WHERE `deleted_at` IS null LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allBook[] = $row;
        }

        return $_allBook;

    }
    public function view(){
        $query="Select * from `birthday` where id=".$this->id;
        $result=mysqli_query($this->conn, $query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }
    public function update(){
        $query="UPDATE `atomicprojectb22`.`birthday` SET `username` ='".$this->username."', `birthday`= '".$this->birthday."' WHERE `id` =".$this->id;
        if(mysqli_query($this->conn, $query)){
            return true;
        }else return false;

    }
    public function delete(){
        $query="DELETE FROM `atomicprojectb22`.`birthday` WHERE `id` =".$this->id;
        if(mysqli_query($this->conn, $query)){
            return true;
        }else return false;
    }

    public function trash(){
        $time=time();
        $query="UPDATE `birthday` SET `deleted_at` ='".$time."' WHERE `id` =".$this->id;
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }
    public function restore(){
        $query="UPDATE `birthday` SET `deleted_at` =NULL WHERE `id` =".$this->id;
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }
    public function trashed(){
        $alltrash=array();
        $query="Select * from `birthday` WHERE  `deleted_at` IS NOT NULL";
        if($result=mysqli_query($this->conn,$query)){
            while($row=mysqli_fetch_assoc($result)){
                $alltrash[]=$row;
            }
        }
        return $alltrash;
    }
    public function restoreMultiple($ids=array()){
        $stringedId= implode(",", $ids);

        $query="UPDATE `birthday` SET `deleted_at` = NULL WHERE `id` IN(".$stringedId.")";
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }

    public function deleteMultiple($ids=array()){
        $stringedId= implode(",", $ids);

        $query="DELETE FROM `birthday` WHERE `id` IN(".$stringedId.")";
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }
}