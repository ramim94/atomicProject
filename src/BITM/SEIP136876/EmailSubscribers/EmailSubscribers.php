<?php
namespace App\BITM\SEIP136876\EmailSubscribers;

use App\utils\Utility;

class EmailSubscribers{
    public $id;
    public $userName;
    public $userEmail;
    public $modified;
    public $created_by;
    public $modified_by;
    public $deleted_at;

    public $conn;

    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","atomicprojectb22") or die("Database Connection Error");
    }


    public function index(){
        $allemail=array();
       $query="Select * from `email` WHERE  `deleted_at` IS NULL";
        if($result=mysqli_query($this->conn,$query)){
           while($row=mysqli_fetch_assoc($result)){
               $allemail[]=$row;
           } 
        }
        return $allemail;
    }
    
    public function trashed(){
        $alltrash=array();
        $query="Select * from `email` WHERE  `deleted_at` IS NOT NULL";
        if($result=mysqli_query($this->conn,$query)){
            while($row=mysqli_fetch_assoc($result)){
                $alltrash[]=$row;
            }
        }
        return $alltrash;
    }
    
    public function prepare($arrayData){
        if(array_key_exists("username",$arrayData)){
            $this->userName=$arrayData['username'];
        }
        if(array_key_exists("id",$arrayData)){
            $this->id=$arrayData['id'];
        }
        if(array_key_exists("email",$arrayData)){
            $this->userEmail=$arrayData['email'];
        }
    }
    
    public function store(){
        $query="INSERT INTO `atomicprojectb22`.`email` (`username`,`email`) VALUES ('".$this->userName."','".$this->userEmail."')";
        if(mysqli_query($this->conn, $query)){
            return true;
        }else return false;
        
    }
    public function view(){
        $query="Select * from `email` where id=".$this->id;
        $result=mysqli_query($this->conn, $query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }
    
    public function edit(){
       $query="Select * from `email` where id=".$this->id;
        $result=mysqli_query($this->conn, $query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }
    public function update(){
        $query="UPDATE `email` SET `email` ='".$this->userEmail."' WHERE `id` =".$this->id;
        if(mysqli_query($this->conn, $query)){
            return true;
        }else return false;

    }
    public function delete(){
        $query="DELETE FROM `atomicprojectb22`.`email` WHERE `email`.`id` =".$this->id;
        
        if(mysqli_query($this->conn, $query)){
            return true;
        }else return false;
    }
    public function trash(){
        $time=time();
        $query="UPDATE `email` SET `deleted_at` ='".$time."' WHERE `id` =".$this->id;
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }
    public function restore(){
        $query="UPDATE `email` SET `deleted_at` =NULL WHERE `id` =".$this->id;
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }
    public function restoreMultiple($ids=array()){
        $stringedId= implode(",", $ids);

        $query="UPDATE `email` SET `deleted_at` =NULL WHERE `id` IN(".$stringedId.")";
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }

    public function deleteMultiple($ids=array()){
        $stringedId= implode(",", $ids);

        $query="DELETE FROM `atomicprojectb22`.`email` WHERE `email`.`id` IN(".$stringedId.")";
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }
    
    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `atomicprojectb22`.`email` WHERE `deleted_at` is null";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }
    public function paginator($pageStartFrom=0,$Limit=5){
        $_allBook = array();
        $query="SELECT * FROM `email` WHERE `deleted_at` IS null LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allBook[] = $row;
        }

        return $_allBook;

    }
}