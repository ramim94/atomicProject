<?php
namespace App\Bitm\SEIP136876\ProfilePicture;

class ProfilePicture{

    public $id;
    public $name;
    public $images;
    public $conn;

    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","atomicprojectb22");
    }

    public function prepare($data){
        if(array_key_exists("id",$data)){
            $this->id=$data['id'];
        }if(array_key_exists("name",$data)){
            $this->name=$data['name'];
        }if(array_key_exists("images",$data)){
            $this->images=$data['images'];
        }
    }

    public function store(){
        $query="INSERT INTO `atomicprojectb22`.`profilepicture` (`name`,`images`) VALUES ('".$this->name."','".$this->images."')";
        $result=mysqli_query($this->conn,$query);
        if($result){
            return true;
        }else return false;
    }
    public function index(){
        $allinfo=array();
        $query="Select * FROM `profilepicture` WHERE `deleted_at` IS NULL";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $allinfo[]=$row;
        }
        return $allinfo;
    }
    public function view(){
        $query="Select * from `profilepicture` where id=".$this->id;
        $result=mysqli_query($this->conn, $query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }

    public function edit(){
        $query="Select * from `profilepicture` where id=".$this->id;
        $result=mysqli_query($this->conn, $query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }
    public function update(){

        if(!empty($this->images) && !is_null($this->images)){
            $query="UPDATE `profilepicture` SET `name` ='".$this->name."', `images` = '".$this->images."' WHERE `id` =".$this->id;
        }else{
            $query="UPDATE `profilepicture` SET `name` = '".$this->name."' WHERE `id` =".$this->id;
        }
        
        if(mysqli_query($this->conn, $query)){
            return true;
        }else return false;
    }
    public function delete(){
        $query="DELETE FROM `profilepicture` WHERE `profilepicture`.`id` =".$this->id;
        if(mysqli_query($this->conn, $query)){
            unlink($_SERVER['DOCUMENT_ROOT'] . "atomicProject/resource/Images/". $this->images);
            return true;
        }else return false;
    }
    
    public function trash(){
        $time=time();
        $query="UPDATE `profilepicture` SET `deleted_at` ='".$time."' WHERE `id` =".$this->id;
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }
    public function trashed(){
        $alltrash=array();
        $query="Select * from `profilepicture` WHERE  `deleted_at` IS NOT NULL";
        if($result=mysqli_query($this->conn,$query)){
            while($row=mysqli_fetch_assoc($result)){
                $alltrash[]=$row;
            }
        }
        return $alltrash;
    }
    public function restore(){
            $query="UPDATE `profilepicture` SET `deleted_at` = NULL WHERE `id` =".$this->id;
            if(mysqli_query($this->conn,$query)){
                return true;
            }else return false;
        
    }

    public function restoreMultiple($ids=array()){
        $stringedId= implode(",", $ids);

        $query="UPDATE `profilepicture` SET `deleted_at` =NULL WHERE `id` IN(".$stringedId.")";
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }
    public function deleteMultiple($ids=array()){
        $stringedId= implode(",", $ids);

        $query="DELETE FROM `atomicprojectb22`.`profilepicture` WHERE `id` IN(".$stringedId.")";
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }
    public function setID($ids=""){
        $this->id=$ids;
        
    }
    
    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `atomicprojectb22`.`profilepicture` WHERE `deleted_at` is null ";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }
    public function paginator($pageStartFrom=0,$Limit=5){
        $_allBook = array();
        $query="SELECT * FROM `profilepicture` WHERE `deleted_at` IS null LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allBook[] = $row;
        }

        return $_allBook;

    }
    
}