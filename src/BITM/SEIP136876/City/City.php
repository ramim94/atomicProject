<?php
namespace App\BITM\SEIP136876\City;
class City{
    public $id;
    public $username;
    public $city;
    public $conn;

    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","atomicprojectb22") or die("Database Connection Error");
    }

    public function prepare($arraydata){
        if(array_key_exists("username",$arraydata)){
            $this->username=$arraydata['username'];
        }if(array_key_exists("id",$arraydata)){
            $this->id=$arraydata['id'];
        }if(array_key_exists("city",$arraydata)){
            $this->city=$arraydata['city'];
        }if(array_key_exists("email",$arraydata)){
            $this->emailto=$arraydata['email'];
        }if(array_key_exists("filterTitle",$arraydata)){
            $this->filterTitle=$arraydata['filterTitle'];
        } if(array_key_exists("filterDesc",$arraydata)) {
            $this->filterDesc = $arraydata['filterDesc'];
        }if(array_key_exists("search",$arraydata)) {
            $this->search = $arraydata['search'];
        }
    }
    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `atomicprojectb22`.`city` WHERE `deleted_at` is null";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }
    public function paginator($pageStartFrom=0,$Limit=5){
        $_allBook = array();
        $query="SELECT * FROM `city` WHERE `deleted_at` IS null LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allBook[] = $row;
        }

        return $_allBook;

    }

    public function store(){
        $query="INSERT INTO `atomicprojectb22`.`city` (`username`,`city`,`toemail`) VALUES ('".$this->username."','".$this->city."','".$this->emailto."')";
        $result=mysqli_query($this->conn,$query);
        if($result){
            return true;
        }else return false;
    }
    public function edit(){
        $query="Select * from `city` where id=".$this->id;
        $result=mysqli_query($this->conn, $query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }
    public function update(){
        $query="UPDATE `atomicprojectb22`.`city` SET `username` ='".$this->username."', `city`= '".$this->city."' WHERE `id` =".$this->id;
        if(mysqli_query($this->conn, $query)){
            return true;
        }else return false;

    }
    public function delete(){
        $query="DELETE FROM `atomicprojectb22`.`city` WHERE `id` =".$this->id;
        if(mysqli_query($this->conn, $query)){
            return true;
        }else return false;
    }

    public function trash(){
        $time=time();
        $query="UPDATE `city` SET `deleted_at` ='".$time."' WHERE `id` =".$this->id;
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }
    public function restore(){
        $query="UPDATE `city` SET `deleted_at` =NULL WHERE `id` =".$this->id;
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }
    public function view(){
        $query="Select * from `city` where id=".$this->id;
        $result=mysqli_query($this->conn, $query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }

    public function trashed(){
        $alltrash=array();
        $query="Select * from `city` WHERE  `deleted_at` IS NOT NULL";
        if($result=mysqli_query($this->conn,$query)){
            while($row=mysqli_fetch_assoc($result)){
                $alltrash[]=$row;
            }
        }
        return $alltrash;
    }
    public function restoreMultiple($ids=array()){
        $stringedId= implode(",", $ids);

        $query="UPDATE `city` SET `deleted_at` = NULL WHERE `id` IN(".$stringedId.")";
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }

    public function deleteMultiple($ids=array()){
        $stringedId= implode(",", $ids);

        $query="DELETE FROM `city` WHERE `id` IN(".$stringedId.")";
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }
    public function index(){
        $allcity=array();
        $whereClause= " 1=1 ";
        if(!empty($this->filterTitle)){
            $whereClause.=" AND city LIKE '%".$this->filterTitle."%'";
        }
        if(!empty($this->filterDesc)){
            $whereClause.=" AND username LIKE '%".$this->filterDesc."%'";
        }
        if(!empty($this->search)){
            $whereClause.=" AND username LIKE '%".$this->search."%' OR city LIKE '%".$this->search."%'";
        }

        $query="Select * FROM `city` WHERE `deleted_at` IS NULL AND ".$whereClause;
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $allcity[]=$row;
        }
        return $allcity;
    }

    public function getAllCity(){
        $allCity = array();
        $query = "SELECT `city` FROM `city` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $allCity[] = $row['city'];
        }
        return $allCity;
    }
}