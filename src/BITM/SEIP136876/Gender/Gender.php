<?php
namespace App\BITM\SEIP136876\Gender;

class Gender{
    public $id;
    public $username;
    public $gender;
    public $conn;

    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","atomicprojectb22") or die("Database Connection Error");
    }

    public function prepare($arraydata){
        if(array_key_exists("username",$arraydata)){
            $this->username=$arraydata['username'];
        }if(array_key_exists("id",$arraydata)){
            $this->id=$arraydata['id'];
        }if(array_key_exists("gender",$arraydata)){
            $this->gender=$arraydata['gender'];
        }
    }

    public function store(){
        $query="INSERT INTO `atomicprojectb22`.`gender` (`username`,`gender`) VALUES ('".$this->username."','".$this->gender."')";
        $result=mysqli_query($this->conn,$query);
        if($result){
            return true;
        }else return false;
    }

    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `atomicprojectb22`.`gender` WHERE `deleted_at` is null ";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }
    public function paginator($pageStartFrom=0,$Limit=5){
        $_allBook = array();
        $query="SELECT * FROM `gender` WHERE `deleted_at` IS null LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allBook[] = $row;
        }

        return $_allBook;

    }
    public function view(){
        $query="Select * from `gender` where id=".$this->id;
        $result=mysqli_query($this->conn, $query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }
    public function update(){
        $query="UPDATE `atomicprojectb22`.`gender` SET `username` ='".$this->username."', `gender`= '".$this->gender."' WHERE `id` =".$this->id;
        if(mysqli_query($this->conn, $query)){
            return true;
        }else return false;

    }
    public function delete(){
        $query="DELETE FROM `atomicprojectb22`.`gender` WHERE `id` =".$this->id;
        if(mysqli_query($this->conn, $query)){
            return true;
        }else return false;
    }

    public function trash(){
        $time=time();
        $query="UPDATE `gender` SET `deleted_at` ='".$time."' WHERE `id` =".$this->id;
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }
    public function restore(){
        $query="UPDATE `gender` SET `deleted_at` =NULL WHERE `id` =".$this->id;
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }
    public function trashed(){
        $alltrash=array();
        $query="Select * from `gender` WHERE  `deleted_at` IS NOT NULL";
        if($result=mysqli_query($this->conn,$query)){
            while($row=mysqli_fetch_assoc($result)){
                $alltrash[]=$row;
            }
        }
        return $alltrash;
    }
    public function restoreMultiple($ids=array()){
        $stringedId= implode(",", $ids);

        $query="UPDATE `gender` SET `deleted_at` = NULL WHERE `id` IN(".$stringedId.")";
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }

    public function deleteMultiple($ids=array()){
        $stringedId= implode(",", $ids);

        $query="DELETE FROM `gender` WHERE `id` IN(".$stringedId.")";
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }
}