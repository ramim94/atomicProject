<?php
namespace App\BITM\SEIP136876\Hobby;


class Hobby{

    public $id;
    public $hobbies;
    public $conn;
    public $hobbylist=array();

    public function __construct(){

       $this->conn=mysqli_connect("localhost","root","","atomicprojectb22");
    }

    public function prepare($data){
        if(array_key_exists("id",$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists("hobby",$data)){
            $this->hobbies=$data['hobby'];
        }
    }
    public function store(){
        $query="INSERT INTO `atomicprojectb22`.`hobby` (`hobbies`) VALUES ('".$this->hobbies."');";
       if(mysqli_query($this->conn,$query)){
           return true;
       }else return false;
    }

    public function index(){
        $query="Select * from `hobby` WHERE `deleted_at` IS NULL";

        $result=mysqli_query($this->conn,$query);
            while($row=mysqli_fetch_assoc($result)){
                $this->hobbylist[]=$row;
            }
        return $this->hobbylist;
    }

    public function view(){
        $query="Select * from `hobby` WHERE id='".$this->id."'";
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }
    public function edit(){
        $query="Select * from `hobby` WHERE id='".$this->id."'";
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_assoc($result);
        $this->hobbylist=explode(",",$row['hobbies']);
        return $this->hobbylist;
    }

    public function update(){
        $query="UPDATE `hobby` SET `hobbies` ='".$this->hobbies."' WHERE `id` =".$this->id;
        if(mysqli_query($this->conn, $query)){
            return true;
        }else return false;

    }
    public function delete(){
        $query="DELETE FROM `atomicprojectb22`.`hobby` WHERE `hobby`.`id` =".$this->id;
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }
    public function restore(){
        $query="UPDATE `hobby` SET `deleted_at` = NULL WHERE `id` =".$this->id;
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }

    public function trash(){
        $time=time();
        $query="UPDATE `hobby` SET `deleted_at` ='".$time."' WHERE `id` =".$this->id;
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }

    public function trashed(){
        $alltrash=array();
        $query="Select * from `hobby` WHERE  `deleted_at` IS NOT NULL";
        if($result=mysqli_query($this->conn,$query)){
            while($row=mysqli_fetch_assoc($result)){
                $alltrash[]=$row;
            }
        }
        return $alltrash;
    }

    public function restoreMultiple($ids=array()){
        $stringedId= implode(",", $ids);

        $query="UPDATE `hobby` SET `deleted_at` =NULL WHERE `id` IN(".$stringedId.")";
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }

    public function deleteMultiple($ids=array()){
        $stringedId= implode(",", $ids);

        $query="DELETE FROM `atomicprojectb22`.`hobby` WHERE `hobby`.`id` IN(".$stringedId.")";
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }
    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `atomicprojectb22`.`hobby` WHERE `deleted_at` is null";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }
    public function paginator($pageStartFrom=0,$Limit=5){
        $_allBook = array();
        $query="SELECT * FROM `hobby` WHERE `deleted_at` IS null LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allBook[] = $row;
        }

        return $_allBook;

    }
}
