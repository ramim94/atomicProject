<?php
namespace App\BITM\SEIP136876\BookTitle;
use App\utils\Utility;

class BookTitle{

    public $allbook=array();

    public $id;
    public $name;
    public $description;
    public $filterTitle;
    public $filterDesc;
    public $search;
    public $deleted_at;
    public $emailto;
    public $conn;

    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","atomicprojectb22") or die("Database Connection Error");
    }
    public function index(){
        $whereClause= " 1=1 ";
        if(!empty($this->filterTitle)){
            $whereClause.=" AND title LIKE '%".$this->filterTitle."%'";
        }
        if(!empty($this->filterDesc)){
            $whereClause.=" AND description LIKE '%".$this->filterDesc."%'";
        }
        if(!empty($this->search)){
            $whereClause.=" AND description LIKE '%".$this->search."%' OR title LIKE '%".$this->search."%'";
        }

        $query="Select * FROM `book` WHERE `deleted_at` IS NULL AND ".$whereClause;
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $this->allbook[]=$row;
        }
        return $this->allbook;
    }
    public function restore(){
        $query="UPDATE `book` SET `deleted_at` =NULL WHERE `id` =".$this->id;
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }

    public function created(){
        return "I am creating form";
    }
    public function store(){
        $query="INSERT INTO `atomicprojectb22`.`book` (`title`,`description`,`toemail`) VALUES ('".$this->name."','".$this->description."','".$this->emailto."')";
        $result=mysqli_query($this->conn,$query);
        if($result){
           return true;
        }else return false;
    }
    public function edit(){
       $query="Select * from `book` where id=".$this->id;
        $result=mysqli_query($this->conn, $query);
        $row=mysqli_fetch_object($result);
        return $row;
    }
    public function update(){
       $query="UPDATE `book` SET `title` ='".$this->name."' WHERE `id` =".$this->id;
        if(mysqli_query($this->conn, $query)){
            return true;
        }else return false;

    }
    public function delete(){
       $query="DELETE FROM `atomicprojectb22`.`book` WHERE `book`.`id` =".$this->id;
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }

    public function view(){
        $query="Select * from `book` where id=".$this->id;
        $result=mysqli_query($this->conn, $query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }
    public function trashed(){
        $query="Select * FROM `book` WHERE `deleted_at` IS NOT NULL";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $this->allbook[]=$row;
        }
        return $this->allbook;
    }
    
    
    public function trash(){
        $time=time();
        $query="UPDATE `book` SET `deleted_at` ='".$time."' WHERE `id` =".$this->id;
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }

    public function restoreMultiple($ids=array()){
        $stringedId= implode(",", $ids);
        
        $query="UPDATE `book` SET `deleted_at` =NULL WHERE `id` IN(".$stringedId.")";
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }
    
    public function deleteMultiple($ids=array()){
        $stringedId= implode(",", $ids);

        $query="DELETE FROM `atomicprojectb22`.`book` WHERE `book`.`id`IN(".$stringedId.")";
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }



    public function prepare($data){
        if(array_key_exists("title",$data)){
        $this->name=$data['title'];
        } if(array_key_exists("description",$data)){
            $this->description=$data['description'];
        } if(array_key_exists("filterTitle",$data)){
            $this->filterTitle=$data['filterTitle'];
        } if(array_key_exists("filterDesc",$data)){
            $this->filterDesc=$data['filterDesc'];
        } if(array_key_exists("id",$data)){
            $this->id=$data['id'];
        }if(array_key_exists("search",$data)){
            $this->search=$data['search'];
        }if(array_key_exists("email",$data)){
            $this->emailto=$data['email'];
        }
    }

    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `atomicprojectb22`.`book` WHERE `deleted_at` is null";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }
    public function paginator($pageStartFrom=0,$Limit=5){
        $_allBook = array();
        $query="SELECT * FROM `book` WHERE `deleted_at` IS null LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allBook[] = $row;
        }

        return $_allBook;

    }
    public function getAllTitle(){
            $_allBook = array();
            $query = "SELECT `title` FROM `book` WHERE `deleted_at` IS NULL";
            $result = mysqli_query($this->conn, $query);
            while ($row = mysqli_fetch_assoc($result)) {
                $_allBook[] = $row['title'];
            }
            return $_allBook;
    }
}