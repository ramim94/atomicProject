<?php
namespace App\Bitm\SEIP136876\Summary;

use App\utils\Utility;

class Summary{
    public $id;
    public $orgName;
    public $summary;
    
    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","atomicprojectb22");
    }

    public function prepare($arrayData){
        if(array_key_exists("orgname",$arrayData)){
            $this->orgName=$arrayData['orgname'];
        }
        if(array_key_exists("id",$arrayData)){
            $this->id=$arrayData['id'];
        }
        if(array_key_exists("summary",$arrayData)){
            $this->summary=$arrayData['summary'];
        }if(array_key_exists("email",$arrayData)){
            $this->emailto=$arrayData['email'];
        }if(array_key_exists("filterTitle",$arrayData)){
            $this->filterTitle=$arrayData['filterTitle'];
        } if(array_key_exists("filterDesc",$arrayData)) {
            $this->filterDesc = $arrayData['filterDesc'];
        }if(array_key_exists("search",$arrayData)) {
            $this->search = $arrayData['search'];
        }
    }
    public function store(){
        $query="INSERT INTO `atomicprojectb22`.`summary` (`orgname`,`summary`,`toemail`) VALUES ('".$this->orgName."','".$this->summary."','".$this->emailto."')";
        if(mysqli_query($this->conn, $query)){
            return true;
        }else return false;

    }
    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `atomicprojectb22`.`summary` WHERE `deleted_at` is null";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }
    public function paginator($pageStartFrom=0,$Limit=5){
        $_allBook = array();
        $query="SELECT * FROM `summary` WHERE `deleted_at` IS null LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allBook[] = $row;
        }

        return $_allBook;

    }
    public function edit(){
        $query="Select * from `summary` where id=".$this->id;
        $result=mysqli_query($this->conn, $query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }
    public function update(){
        $query="UPDATE `summary` SET `summary` ='".$this->summary."' WHERE `id` =".$this->id;
        if(mysqli_query($this->conn, $query)){
            return true;
        }else return false;

    }
    public function view(){
        $query="Select * from `atomicprojectb22`.`summary` where id=".$this->id;
        $result=mysqli_query($this->conn, $query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }
    public function delete(){
        $query="DELETE FROM `atomicprojectb22`.`summary` WHERE `id` =".$this->id;

        if(mysqli_query($this->conn, $query)){
            return true;
        }else return false;
    }
    public function trash(){
        $time=time();
        $query="UPDATE `summary` SET `deleted_at` ='".$time."' WHERE `id` =".$this->id;
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }
    public function trashed(){
        $alltrash=array();
        $query="Select * from `summary` WHERE  `deleted_at` IS NOT NULL";
        if($result=mysqli_query($this->conn,$query)){
            while($row=mysqli_fetch_assoc($result)){
                $alltrash[]=$row;
            }
        }
        return $alltrash;
    }
    public function restore(){
        $query="UPDATE `summary` SET `deleted_at`=NULL WHERE `id` =".$this->id;
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }
    public function restoreMultiple($ids=array()){
        $stringedId= implode(",", $ids);

        $query="UPDATE `summary` SET `deleted_at` =NULL WHERE `id` IN(".$stringedId.")";
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }

    public function deleteMultiple($ids=array()){
        $stringedId= implode(",", $ids);

        $query="DELETE FROM `atomicprojectb22`.`summary` WHERE `id` IN(".$stringedId.")";
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }
    public function index(){
        $alldata=array();
        $whereClause= " 1=1 ";
        if(!empty($this->filterTitle)){
            $whereClause.=" AND orgname LIKE '%".$this->filterTitle."%'";
        }
        if(!empty($this->filterDesc)){
            $whereClause.=" AND summary LIKE '%".$this->filterDesc."%'";
        }
        if(!empty($this->search)){
            $whereClause.=" AND summary LIKE '%".$this->search."%' OR orgname LIKE '%".$this->search."%'";
        }

        $query="Select * FROM `summary` WHERE `deleted_at` IS NULL AND ".$whereClause;
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $alldata[]=$row;
        }
        return $alldata;
    }

    public function getAllSummary(){
        $allSummary = array();
        $query = "SELECT `summary` FROM `summary` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $allSummary[] = $row['summary'];
        }
        return $allSummary;
    }

}